/*
** play.c for lemipc in /home/faraday/projects/C/lemipc
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Fri Mar  6 13:19:11 2015 raphael elkaim
** Last update Sat Mar  7 19:50:21 2015 arthur
*/

#include <unistd.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "lemipc.h"

int	same_players(char **table)
{
  int	i;
  int	j;
  char	oth;
  int	count;

  i = 0;
  count = 0;
  oth = (g_game.team == A) ? 'a' : 'b';
  while (i < ROW)
    {
      j = 0;
      while (j < ROW)
	{
	  if (table[i][j] == oth)
	    ++count;
	  ++j;
	}
      ++i;
    }
  return (count);
}

void	find_spot(char **table, int *nposX, int *nposY)
{
  *nposY = random() % ROW;
  *nposX = random() % ROW;
  if (check(table, *nposX, *nposY))
    {
      sem_inc();
      exit(EXIT_SUCCESS);
    }
}

void	delete_resources(int mem)
{
  semctl(g_sid, IPC_RMID, 0);
  shmctl(mem, IPC_RMID, NULL);
  //  msgctl(msgid, IPC_RMID, NULL);
}

int	get_free_squares(char **table)
{
  int	i;
  int	j;
  int	res;

  i = 0;
  res = 0;
  while (i < ROW)
    {
      j = 0;
      while (j < ROW)
	{
	  if (table[i][j] == '0')
	    ++res;
	  ++j;
	}
      ++i;
    }
  return (res);
}

void	play_it(char **table, int mem)
{
  while (1)
    {
      sleep(1);
      sem_dec();
      ia_play(table, mem);
      sem_inc();
    }
}
