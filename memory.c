/*
** memory.c for lemipc in /home/faraday/projects/C/lemipc
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Fri Feb 27 15:29:41 2015 raphael elkaim
** Last update Fri Mar  6 15:24:34 2015 raphael elkaim
*/

#include <stdlib.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include "lemipc.h"

char	**init_table(int memid, ebool is_begin)
{
  char	**table;
  void	*test;
  int	*addr;
  int	i;

  i = 0;
  if ((test = shmat(memid, NULL, 0)) == (void *)-1)
    exit(EXIT_FAILURE);
  addr = test + INT_ADDR;
  if (is_begin == true)
    *addr = 0;
  else
    *addr += 1;
  if ((table = malloc(ROW * sizeof(void *))) == NULL)
    {
      perror("malloc:");
      exit(EXIT_FAILURE);
    }
  while (i < ROW)
    {
      table[i] = (void *)(test + 20 * i);
      ++i;
    }
  return (table);
}

char 	**make_table(int memid, ebool is_begin)
{
  char	**table;
  int	i;
  int	j;

  i = 0;
  j = 0;
  sem_dec();
  table = init_table(memid, is_begin);
  while (i < ROW)
    {
      while (j < ROW)
	{
	  table[i][j] = '0';
	  ++j;
	}
      ++i;
      j = 0;
    }
  sem_inc();
  return (table);
}
