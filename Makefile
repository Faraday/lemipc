##
## Makefile for makefile in /home/amstuta/rendu/philosophes
##
## Made by arthur
## Login   <amstuta@epitech.net>
##
## Started on  Tue Feb 17 11:10:43 2015 arthur
## Last update Fri Mar  6 17:04:41 2015 raphael elkaim
##

CC	=	gcc

CFLAGS	=	-Wall -Wextra -Werror -I ./gui/include

RM	=	rm -f

SRCS	=	main.c \
		memory.c \
		ia.c \
		sem.c \
		play.c \
		ipc.c

GUI_SRC	=	gui/main_sdl.c \
		gui/blit.c

NAME	=	lemipc

DISP	=	disp

OBJS	=	$(SRCS:.c=.o)

GUI_OBJ	=	$(GUI_SRC:.c=.o)

all:	$(NAME) $(DISP)

$(NAME):$(OBJS)
	$(CC) $(OBJS) -o $(NAME)

$(DISP):$(GUI_OBJ)
	$(CC) $(GUI_OBJ) -o $(DISP) -lSDL -L./gui/lib/

clean:
	$(RM) $(OBJS)
	$(RM) $(GUI_OBJ)

fclean:	clean
	$(RM) $(NAME)
	$(RM) $(DISP)

re:	clean fclean all

.PHONY:	all clean fclean re
