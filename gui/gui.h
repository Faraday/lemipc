#ifndef GUI_H_
# define GUI_H_

# define ROW		20
# define TAB_SIZE	(sizeof(char) * ROW * ROW)

void			blit_img(int x,
				 int y,
				 SDL_Surface *screen,
				 SDL_Surface *img);


#endif
