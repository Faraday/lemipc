/*
** main.c for lemipc gui in /home/faraday/projects/C/lemipc/gui
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Mon Mar  2 15:47:30 2015 raphael elkaim
** Last update Fri Mar  6 17:05:30 2015 raphael elkaim
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "SDL.h"
#include "gui.h"

SDL_Surface		*load_images(const char *to_load)
{
  SDL_Surface		*new_image;

  new_image = SDL_LoadBMP(to_load);
  if (!new_image)
    exit(EXIT_FAILURE);
  return (new_image);
}

char			**init_table(int memid)
{
  char			**table;
  void			*test;
  int			i;

  i = 0;
  if ((test = shmat(memid, NULL, 0)) == (void *)-1)
    {
      perror("shmat:");
      exit(EXIT_FAILURE);
    }
  if ((table = malloc(ROW * sizeof(void *) + 4)) == NULL)
    {
      perror("malloc:");
      exit(EXIT_FAILURE);
    }
  while (i < ROW)
    {
      table[i] = (void *)(test + 20 * i);
      ++i;
    }
  return (table);
}


char	**get_shared_memory(int key)
{
  int	mem;
  char	**table;
  
  if ((mem = shmget(key, TAB_SIZE, SHM_R | SHM_W)) == -1)
    {
      printf("error: no game is running!\n");
      exit(EXIT_FAILURE);
    }
  else
    table = init_table(mem);
  return (table);
}

void		show_table(SDL_Surface *screen, char **table)
{
  SDL_Surface	*square[3];
  int		i;
  int		j;

  i = 0;
  square[0] = load_images("./gui/media/realempy.bmp");
  square[1] = load_images("./gui/media/Empty.bmp");
  square[2] = load_images("./gui/media/greenTeam.bmp");
  while (i < 20)
    {
      j = 0;
      while (j < 20)
	{
	  if (table[i][j] == '0')
	    blit_img(j * 20, i * 20, screen, square[0]);
	  if (table[i][j] == 'a')
	    blit_img(j * 20, i * 20, screen, square[1]);
	  if (table[i][j] == 'b')
	    blit_img(j * 20, i * 20, screen, square[2]);
	  ++j;
	}
      ++i;
    }
  SDL_Flip(screen);
}

int		main()
{
  SDL_Surface	*screen;
  SDL_Event	event;
  char		**table;
  int		key;
  
  key = ftok("./lemipc", 42);
  if( SDL_Init( SDL_INIT_VIDEO ) == -1 )
    return (EXIT_FAILURE);
  atexit(SDL_Quit);
  screen = SDL_SetVideoMode(1000, 400, 16, SDL_HWSURFACE);
  if( screen == NULL )
    return (EXIT_FAILURE);
  table = get_shared_memory(key);
  while (1)
    {
      usleep(1000);
      while(SDL_PollEvent(&event)) {
	if(event.type == SDL_QUIT) {
	  return (EXIT_SUCCESS);
	}
      }
      show_table(screen, table);      
    }
    return (EXIT_SUCCESS);
}
