/*
** blit.c for lemipc in /home/faraday/projects/C/lemipc/gui
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Fri Mar  6 16:39:49 2015 raphael elkaim
** Last update Fri Mar  6 17:05:18 2015 raphael elkaim
*/

#include <stdio.h>
#include "SDL.h"
#include "gui.h"

void			blit_img(int x,
				 int y,
				 SDL_Surface *screen,
				 SDL_Surface *img)
{
  SDL_Rect offset;

  offset.x = x;
  offset.y = y;
  if (SDL_BlitSurface(img, NULL, screen, &offset) == -1)
    printf("error:blit failed\n");
}
