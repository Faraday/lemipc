/*
** ipc.c for lemipc in /home/faraday/projects/C/lemipc
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Fri Mar  6 15:08:20 2015 raphael elkaim
** Last update Fri Mar  6 15:15:34 2015 raphael elkaim
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include "lemipc.h"

int	make_shared_mem(int key)
{
  int	mem;
  
  mem = shmget(key, TAB_SIZE + 4, SHM_R | SHM_W | IPC_CREAT);
  if (mem < 0)
    {
      perror("shmget:");
      exit(EXIT_FAILURE);
    }
  return (mem);
}

void	make_sem(int key)
{
  g_sid = semget(key, 1, SHM_R | SHM_W | IPC_CREAT);
  if (g_sid < 0)
    {
      perror("semget:");
      exit(EXIT_FAILURE);
    }
  if (semctl(g_sid, 0, SETVAL, 1) == -1)
    {
      perror("semctl:");
      exit(EXIT_FAILURE);
    }
}

int	make_msg(int key)
{
  int	message;
  
  message = msgget(key, IPC_CREAT | SHM_R | SHM_W);
  if (message < 0)
    {
      perror("msgget:");
      exit(EXIT_FAILURE);
    }
  return (message);
}

int	send_msg(int msg, int nb)
{
  t_msg	message;
  int	ret;
  int	i;

  message.type = 1;
  message.text[0] = 'O';
  message.text[1] = 0;
  i = 0;
  while (i < nb)
    {
      ret = msgsnd(msg, &message, 2, IPC_NOWAIT);
      if (ret < 0)
	{
	  perror("msgsnd:");
	  exit(EXIT_FAILURE);
	}
      ++i;
    }
  return (0);
}

int	rcv_message(int msg)
{
  t_msg	message;
  int	ret;
  int	time;

  time = 0;
  if (msg < 0)
    {
      perror("msgget:");
      exit(EXIT_FAILURE);
    }
  while (time < 10)
    {
      ret = msgrcv(msg, &message, 2, 1, IPC_NOWAIT);
      if (ret != -1)
	break ;
      ++time;
      sleep(1);
    }
  if (ret < 0)
    {
      perror("msgrcv:");
      exit(EXIT_FAILURE);
    }
  return (0);
}
