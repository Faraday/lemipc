/*
** main.c for lemipc in /home/faraday/projects/C/lemipc
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Wed Feb 25 14:50:17 2015 raphael elkaim
** Last update Sat Mar  7 19:50:18 2015 arthur
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include "lemipc.h"

t_play		g_game;
int		g_sid;
struct sembuf	g_op = {0, -1, 0};

void	game_init(char **table)
{
  char	team;
  int	*nb;

  nb = (int *)(table[0] + INT_ADDR);
  if (*nb > 19)
    {
      printf("Error: too many players\n");
      exit(EXIT_FAILURE);
    }
  g_game.posX = *nb;
  g_game.posY = (*nb % 2) * 19;
  g_game.team = *nb % 2;
  team = g_game.team == A ? 'a' : 'b';
  sem_dec();
  if (check(table, g_game.posX, g_game.posY))
    {
      sem_inc();
      exit(EXIT_FAILURE);
    }
  table[g_game.posY][g_game.posX] = team;
  sem_inc();
}

void	create_players(char **av)
{
  int	i;
  pid_t	pid;

  i = 0;
  while (i < 9)
    {
      sleep(1);
      pid = fork();
      if (!pid)
	execl(av[0], av[0], NULL);
      else if (pid == -1)
	{
	  perror("fork:");
	  exit(EXIT_FAILURE);
	}
      else
	printf("created player %d\n", i + 2);
      ++i;
    }
}

void	create_shared_memory(int key, char **av)
{
  int	mem;
  int	msg;
  char	**table;
  
  if ((mem = shmget(key, TAB_SIZE + 4, SHM_R | SHM_W)) == -1)
    {
      printf("wat\n");
      mem = make_shared_mem(key);
      make_sem(key);
      table = make_table(mem, true);
      game_init(table);
      msg = make_msg(key);
      create_players(av);
      send_msg(msg, 9);
      play_it(table, mem);
    }
  else
    {
      if ((g_sid = semget(key, 1, SHM_R | SHM_W)) < 0)
	{
	  perror("semget:");
	  return ;
	}
      msg =  msgget(key, SHM_R | SHM_W);
      table = init_table(mem, false);
      game_init(table);
      rcv_message(msg);
      play_it(table, mem);
    }
}

int	key_init(char *path)
{
  return (ftok(path, 42));
}

int	main(int ac, char **av)
{
  int	key;
  
  (void)ac;
  key = key_init(av[0]);
  if (key < 0)
    {
      printf("error: failed to initialize key.\n");
      return (EXIT_FAILURE);
    }
  srandom(time(0));
  create_shared_memory(key, av);
  return (EXIT_SUCCESS);
}
