/*
** ia.c for ia in /home/amstuta/rendu/lemipc
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Mon Mar  2 14:23:02 2015 arthur
** Last update Fri Mar  6 15:59:15 2015 raphael elkaim
*/

#include <stdlib.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include "lemipc.h"

ebool	check_square(char **tab, int posX, int posY)
{
  int	count;
  char	other_team;

  count = 0;
  other_team = (g_game.team == A) ? 'b' : 'a';
  count += (posX > 0 && posY > 0 && tab[posY - 1][posX - 1] == other_team);
  count += (posY > 0 && tab[posY - 1][posX] == other_team);
  count += (posY > 0 && posX < 19 && tab[posY - 1][posX + 1] == other_team);
  count += (posX > 0 && tab[posY][posX - 1] == other_team);
  count += (posX < 19 && tab[posY][posX + 1] == other_team);
  count += (posY < 19 && posX > 0 && tab[posY + 1][posX - 1] == other_team);
  count += (posY < 19 && tab[posY + 1][posX] == other_team);
  count += (posX < 19 && posY < 19 && tab[posY + 1][posX + 1] == other_team);
  if (count >= 2)
    return (false);
  return (true);
}

ebool	check(char **table, int posX, int posY)
{
  if (check_square(table, posX, posY))
    {
      g_game.posX = g_game.posY = DEAD;
      return (false);
    }
  return (true);
}

int	other_players(char **table)
{
  int	i;
  int	j;
  char	oth;
  int	count;

  i = 0;
  count = 0;
  oth = (g_game.team == A) ? 'b' : 'a';
  while (i < ROW)
    {
      j = 0;
      while (j < ROW)
	{
	  if (table[i][j] == oth)
	    ++count;
	  ++j;
	}
      ++i;
    }
  return (count);
}

int	is_last(char **table)
{
  int	i;
  int	j;
  int	count;

  i = 0;
  count = 0;
  while (i < ROW)
    {
      j = 0;
      while (j < ROW)
	{
	  if (table[i][j] != '0')
	    ++count;
	  ++j;
	}
      ++i;
    }
  return (count);
}

void	ia_play(char **table, int mem)
{
  int	nposY;
  int	nposX;
  char	team;

  team = g_game.team == A ? 'a' : 'b';
  table[g_game.posY][g_game.posX] = '0';
  nposY = random() % ROW;
  nposX = random() % ROW;
  if (check(table, nposX, nposY))
    inc_exit(EXIT_SUCCESS);
  while (table[nposY][nposX] != '0')
    find_spot(table, &nposX, &nposY);
  table[nposY][nposX] = team;
  g_game.posX = nposX;
  g_game.posY = nposY;
  if (!other_players(table) || same_players(table) == 1)
    {
      table[nposY][nposX] = '0';
      if (!is_last(table))
	delete_resources(mem);
      inc_exit(EXIT_SUCCESS);
    }
}
