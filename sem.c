/*
** sem.c for lemipc in /home/faraday/projects/C/lemipc
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Wed Mar  4 14:16:15 2015 raphael elkaim
** Last update Fri Mar  6 16:02:39 2015 raphael elkaim
*/

#include <sys/sem.h>
#include <stdlib.h>
#include <stdio.h>
#include "lemipc.h"

void	sem_dec()
{
  g_op.sem_num = g_sid;
  g_op.sem_op = -1;
  if (semop(g_sid, &g_op, 1) == -1)
    {
      perror("semop:");
      exit(EXIT_FAILURE);
    }
}

void	sem_inc()
{
  g_op.sem_op = 1;
  semop(g_sid, &g_op, 1);
}

void	inc_exit(int ex)
{
  sem_inc();
  exit(ex);
}
