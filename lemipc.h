/*
** lemipc.h for lemipc in /home/faraday/projects/C/lemipc
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Fri Feb 27 15:11:25 2015 raphael elkaim
** Last update Sat Mar  7 19:51:08 2015 arthur
*/

#ifndef LEMIPC_H_
# define LEMIPC_H_

# define ROW		20
# define TAB_SIZE	(sizeof(char) * ROW * ROW)
# define INT_ADDR	(sizeof(void*)) * TAB_SIZE
# define DEAD		-1

typedef enum
  {
			true,
			false
  }			ebool;

typedef	enum
  {
			A,
			B
  }			eTeam;

typedef struct		s_play
{
  int			posX;
  int			posY;
  eTeam			team;
  int			nb;
}			t_play;

typedef struct		s_msg
{
  long			type;
  char			text[2];
}			t_msg;

extern t_play		g_game;
extern int		g_sid;
extern struct sembuf	g_op;

void			sem_inc();
void			sem_dec();
void			game_init(char **table);
void			ia_play(char **, int mem);
void			find_spot(char **table, int *nposX, int *nposY);
int			same_players(char **table);
int			make_shared_mem(int);
void			make_sem(int);
int			make_msg(int);
int			send_msg(int, int);
int			rcv_message(int);
void			delete_resources(int mem);
ebool			check(char **, int, int);
void			inc_exit(int);
void			play_it(char **, int);
char			**make_table(int memid, ebool is_begin);
char			**init_table(int memid, ebool is_begin);
ebool			check_square(char **tab, int, int);

#endif
